import java.io.FileNotFoundException;
import java.util.*;

class Poker {

    public static void main(String[] args) throws FileNotFoundException {
        testCase1();
        testCase2();
        testCase3();
        testCase4();
        testCase5();
        testCase6();
        testCase7();
        play();
    }


    //////////////////////////////////////
    // Tests


    public static void testCase1() {
        PokerHand h1 = new PokerHand("5H 5C 6S 7S KD");
        PokerHand h2 = new PokerHand("2C 3S 8S 8D TD");
        PokerCompare p = new PokerCompare();
        if( p.compare( h1, h2 ) > 0 ) {
            System.out.println("Test 1 correct.");
        } else {
            System.out.println("Test 1 failed.");
        }
    }

    public static void testCase2() {
        PokerHand h1 = new PokerHand("5D 8C 9S JS AC");
        PokerHand h2 = new PokerHand("2C 5C 7D 8S QH");
        PokerCompare p = new PokerCompare();
        if(p.compare(h1,h2)<0) {
            System.out.println("Test 2 correct.");
        } else {
            System.out.println("Test 2 failed.");
        }
    }


    public static void testCase3() {
        PokerHand h1 = new PokerHand("2D 9C AS AH AC");
        PokerHand h2 = new PokerHand("3D 6D 7D TD QD");
        PokerCompare p = new PokerCompare();
        if(p.compare(h1,h2)>0) {
            System.out.println("Test 3 correct.");
        } else {
            System.out.println("Test 3 failed.");
        }
    }


    public static void testCase4() {
        PokerHand h1 = new PokerHand("4D 6S 9H QH QC");
        PokerHand h2 = new PokerHand("3D 6D 7H QD QS");
        PokerCompare p = new PokerCompare();
        if(p.compare(h1,h2)<0) {
            System.out.println("Test 4 correct.");
        } else {
            System.out.println("Test 4 failed.");
        }
    }


    public static void testCase5() {
        PokerHand h1 = new PokerHand("2H 2D 4C 4D 4S");
        PokerHand h2 = new PokerHand("3C 3D 3S 9S 9D");
        PokerCompare p = new PokerCompare();
        if(p.compare(h1,h2)<0) {
            System.out.println("Test 5 correct.");
        } else {
            System.out.println("Test 5 failed.");
        }
    }


    public static void testCase6() {
        PokerHand h1 = new PokerHand("AH KH QH JH TH");
        PokerHand h2 = new PokerHand("3C 3D 3S 9S 9D");
        PokerCompare p = new PokerCompare();
        //System.out.println(h1+"\t\t"+h2);
        if(p.compare(h1,h2)<0) {
            System.out.println("Test 6 correct.");
        } else {
            System.out.println("Test 6 failed.");
        }
    }

    public static void testCase7() {
        PokerHand h1 = new PokerHand("AH QD JD TS 8S");
        PokerHand h2 = new PokerHand("6H 5S 4C 3S 2C");
        PokerCompare p = new PokerCompare();
        //System.out.println(h1+"\t\t"+h2);
        if(p.compare(h1,h2)>0) {
            System.out.println("Test 7 correct.");
        } else {
            System.out.println("Test 7 failed.");
        }
    }





    public static void play()  {
        Scanner scanner = new Scanner(System.in);
        List<String> pokerHandsStrings = new ArrayList<String>();
        do {
            pokerHandsStrings.add(scanner.nextLine());
        }
        while( scanner.hasNextLine());
        scanner.close();
        LinkedList<PokerHand> player1 = new LinkedList<PokerHand>();
        LinkedList<PokerHand> player2 = new LinkedList<PokerHand>();


        for (String pHand:
                pokerHandsStrings) {



            int middleIndex = pHand.length()/2;


            String hand1 = pHand.substring(0, middleIndex);
            String hand2 = pHand.substring(middleIndex+1, pHand.length());

            player1.add(new PokerHand(hand1));
            player2.add(new PokerHand(hand2));
        }

        // Now count the number of wins for player 1
        int p1w = 0;
        int p2w =0;
        PokerCompare p = new PokerCompare();
        for(int i=0; i<player1.size(); i++) {

            if( p.compare(player1.get(i), player2.get(i)) < 0 ) {
                System.out.println("Hand "+i+": Player 1 wins.");
                p1w++;
            } else {
                System.out.println("Hand "+i+": Player 2 wins.");
                p2w++;
            }
        }

        System.out.println("Player 1: " + p1w +" hands");
        System.out.println("Player 2: " + p2w +" hands");

    }
}

class PokerHand implements Comparable<PokerHand> {

    public static final String[] OUTCOMES = {"high","one","two","three","straight","flush","full house","four","straight flush","royal flush"};
    public static final char[] VALUES = {'2','3','4','5','6','7','8','9','T','J','Q','K','A'};
    public static final char[] SUITS = {'S','C','D','H'};

    private class Cards {
        char suit;
        char value;
        public Cards(char suit,char value) { this.suit=suit; this.value=value; }
        public char getSuit() { return suit; }
        public char getValue() { return value; }
        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            sb.append(suit);
            return sb.toString();
        }
    }


    ///////////////////////////////
    // Implement a poker hand

    // The list of five Cards that are dealt to a given player
    LinkedList<Cards> hand;

    // Final cards that matter to the outcome of the game
    LinkedList<Cards> finallyhand;


    // Keep track of various quantities in the hand
    int nSuits;
    int nValues;
    int nPairs;
    int nThrees;


    public PokerHand(String handStr) {
        hand = new LinkedList<Cards>();
        finallyhand = new LinkedList<Cards>();
        String[] tokens = handStr.split(" ");
        // assume exactly 5 tokens, otherwise you're a masochist
        for(int i=0; i<tokens.length; i++) {
            char suit  = tokens[i].charAt(1);
            char value = tokens[i].charAt(0);
            hand.add( new Cards(suit, value) );
        }
        countSuits();
        countValues();
        countPairs();
        countTriplets();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for(Cards c : hand) {
            sb.append(c.getValue());
            sb.append(c.getSuit());
            sb.append(" ");
        }
        sb.append( OUTCOMES[this.getOutcome()] );
        return sb.toString();
    }

    /** Card comparator: compare cards by face value. */
    private class ValueComparator implements Comparator<Cards> {
        public int compare(Cards c1, Cards c2) {
            int c1outcome = indexOf(VALUES,c1.value);
            int c2outcome = indexOf(VALUES,c2.value);
            // swapping the normal order so bigger cards come first
            return (c2outcome-c1outcome);
        }
    }

    /** Card comparator: compare cards by suit. */
    private class SuitComparator implements Comparator<Cards> {
        public int compare(Cards c1, Cards c2) {
            int c1outcome = indexOf(SUITS,c1.suit);
            int c2outcome = indexOf(SUITS,c1.suit);
            // swapping the normal order so bigger cards come first
            return (c2outcome-c1outcome);
        }
    }





    /** Compare two PokerHand objects based on outcome, or if tie, on face value. */
    public int compareTo(PokerHand other) {

        int hand1outcome = this.getOutcome();
        int hand2outcome = other.getOutcome();

        if(hand1outcome==hand2outcome) {
            // Resolve by high card.
            // First, resolve by high card in finallyhand,
            // the cards that were actually important to the
            // final outcome.
            // If those are tied, resolve by high card in hand.

            ValueComparator comp = new ValueComparator();
            Collections.sort(this.finallyhand, comp);
            Collections.sort(other.finallyhand, comp);

            Iterator<Cards> carditer1 = this.finallyhand.iterator();
            Iterator<Cards> carditer2 = other.finallyhand.iterator();

            // Break ties by highest finallyhand cards
            while(carditer1.hasNext() && carditer2.hasNext()) {
                Cards c1 = carditer1.next();
                Cards c2 = carditer2.next();
                if(c1.getValue()!=c2.getValue()) {
                    return comp.compare(c1,c2);
                }
            }

            // If we get here, it's something like
            // two pairs that are tied.
            // Use the rest of the hand.

            carditer1 = this.hand.iterator();
            carditer2 = other.hand.iterator();

            // Break ties by highest finallyhand cards
            while(carditer1.hasNext() && carditer2.hasNext()) {
                Cards c1 = carditer1.next();
                Cards c2 = carditer2.next();
                if(c1.getValue()!=c2.getValue()) {
                    return comp.compare(c1,c2);
                }
            }

            // Well sheeeyut
            return 0;

        } else {
            // swapping the normal order so bigger cards come first
            return (hand2outcome-hand1outcome);
        }

    }






    /** Count number of unique suits. */
    protected void countSuits() {
        this.nSuits = 0;
        Set<Character> suits = new HashSet<Character>();
        for(Cards c : hand) {
            suits.add(c.getValue());
        }
        this.nSuits = suits.size();
    }

    /** Count number of unique values. */
    protected void countValues() {
        this.nValues = 0;
        Set<Character> values = new HashSet<Character>();
        for(Cards c : hand) {
            values.add(c.getValue());
        }
        this.nValues = values.size();
    }

    /** Count pairs. */
    protected void countPairs() {
        this.nPairs = 0;

        // Sort by face value
        ValueComparator comp = new ValueComparator();
        Collections.sort(hand, comp);

        // Count pairs, not triples
        for(int i=0; i+1<hand.size(); i++) {
            Cards ci = hand.get(i);
            Cards cip1 = hand.get(i+1);
            if( ci.getValue() == cip1.getValue() ) {
                nPairs++;
                try {
                    Cards cip2 = hand.get(i+2);
                    if( ci.getValue() == cip2.getValue() ) {
                        nPairs--;
                    }
                } catch(IndexOutOfBoundsException e){
                    // its cool
                }
                // Skip new pair
                i++;
            }
        }
    }

    /** Count three of a kind occurrences. */
    protected void countTriplets() {
        nThrees = 0;

        // Sort by face value
        ValueComparator comp = new ValueComparator();
        Collections.sort(hand, comp);

        // Count pairs, not triples
        for(int i=0; i+2<hand.size(); i++) {
            if( hand.get(i).getValue()==hand.get(i+1).getValue()
                    && hand.get(i).getValue()==hand.get(i+2).getValue() ) {
                nThrees++;
                try {
                    if(hand.get(i).getValue()==hand.get(i+3).getValue()) {
                        nThrees--;
                    }
                } catch(IndexOutOfBoundsException e){
                    // its cool
                }
                // Skip new triplet
                i++;
                i++;
            }
        }
    }




    /** Check for royal flush. */
    public boolean hasRoyalFlush() {
        // Sort by face value
        ValueComparator comp = new ValueComparator();
        Collections.sort(hand, comp);

        // Each card should have same suit
        char whichSuit = hand.get(0).getSuit();
        // We need one of each
        int na = 0, nk = 0, nq = 0, nj = 0, nd = 0;
        for(Cards c : hand) {
            if(c.getSuit()!=whichSuit) return false;
            if(c.getValue()=='A') na++;
            if(c.getValue()=='K') nk++;
            if(c.getValue()=='Q') nq++;
            if(c.getValue()=='J') nj++;
            if(c.getValue()=='T') nd++;
        }
        if( na==1 && nk==1 && nq==1 && nj==1 && nd==1 ) {
            finallyhand =this.doSafeCast(hand.clone(),Cards.class);
            return true;
        } else {
            return false;
        }
    }

    /** Check for straight flush - same suit cards in order. */
    public boolean hasStraightFlush() {
        // If we have a straight or a flush, these will take care of finallyhand for us.
        if( hasStraight() && hasFlush() ) {
            return true;
        } else {
            return false;
        }
    }

    /** Four of a kind */
    public boolean hasFour() {
        if(nValues==2 && nPairs==1 && nThrees==0){

            // Only four of a kind cards should go in finallyhand
            for(int i=0; i<hand.size(); i++) {
                int duper = 0;
                for(int j=0; j<hand.size(); j++) {
                    if(i==j) {
                        continue;
                    }
                    if(hand.get(i).getValue()==hand.get(j).getValue()) {
                        duper++;
                    }
                }
                if(duper==4) {
                    finallyhand.add(hand.get(i));
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /** Full house. */
    public boolean hasFullHouse() {
        if(nValues==2 && nPairs==1 && nThrees==1){

            // Only three of a kind cards should go in finallyhand
            for(int i=0; i<hand.size(); i++) {
                int duper = 0;
                for(int j=0; j<hand.size(); j++) {
                    if(i==j) {
                        continue;
                    }
                    if(hand.get(i).getValue()==hand.get(j).getValue()) {
                        duper++;
                    }
                }
                if(duper==2) {
                    finallyhand.add(hand.get(i));
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /** Check for flush - matching straights. */
    public boolean hasFlush() {
        // Add suits to a set, size should be 1
        Set<Character> suits = new HashSet<Character>();
        for(Cards c : hand) {
            suits.add(c.getSuit());
        }
        if(suits.size()==1) {
            finallyhand = this.doSafeCast(hand.clone(),Cards.class);
            return true;
        } else {
            return false;
        }
    }

    /** Check for straight - cards in order. */
    public boolean hasStraight() {
        // Sort by face value
        ValueComparator comp = new ValueComparator();
        Collections.sort(hand, comp);


        int priorCardIndex, thisCardIndex;

        Iterator<Cards> iter = hand.iterator();
        Cards mycard = iter.next();


        priorCardIndex = indexOf(VALUES, mycard.getValue());
        while(iter.hasNext()) {
            mycard = iter.next();
            thisCardIndex = indexOf(VALUES, mycard.getValue());
            if( thisCardIndex != (priorCardIndex-1) ) {
                return false;
            }
            priorCardIndex = thisCardIndex;
        }
        finallyhand =this.doSafeCast(hand.clone(),Cards.class);
        return true;

    }

    /** Check for three pairs */
    public boolean hasThree() {
        if(nThrees>0) {

            // Only three of a kind cards should go in finallyhand
            for(int i=0; i<hand.size(); i++) {
                int duper = 0;
                for(int j=0; j<hand.size(); j++) {
                    if(i==j) {
                        continue;
                    }
                    if(hand.get(i).getValue()==hand.get(j).getValue()) {
                        duper++;
                    }
                }
                if(duper==2) {
                    finallyhand.add(hand.get(i));
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /** Check for two pairs */
    public boolean hasTwo() {
        if(nPairs==2) {

            // Only pair cards should go in finallyhand
            for(int i=0; i<hand.size(); i++) {
                int duper = 0;
                for(int j=0; j<hand.size(); j++) {
                    if(i==j) {
                        continue;
                    }
                    if(hand.get(i).getValue()==hand.get(j).getValue()) {
                        duper++;
                    }
                }
                if(duper==1) {
                    finallyhand.add(hand.get(i));
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /** Check for one pair */
    public boolean hasOne() {
        if(nPairs==1) {

            // Only pair cards should go in finallyhand
            for(int i=0; i<hand.size(); i++) {
                int duper = 0;
                for(int j=0; j<hand.size(); j++) {
                    if(i==j) {
                        continue;
                    }
                    if(hand.get(i).getValue()==hand.get(j).getValue()) {
                        duper++;
                    }
                }
                if(duper==1) {
                    finallyhand.add(hand.get(i));
                }
            }

            return true;
        } else {
            return false;
        }
    }




    /** Get string indicating outcome. */
    public int getOutcome() {
        // Check in order of rank:
        // royal flush
        if(hasRoyalFlush()) {
            return indexOf(OUTCOMES,"royal flush");
        } else if(hasStraightFlush()) {
            return indexOf(OUTCOMES,"straight flush");
        } else if(hasFour()) {
            return indexOf(OUTCOMES,"four");
        } else if(hasFullHouse()) {
            return indexOf(OUTCOMES,"full house");
        } else if(hasFlush()) {
            return indexOf(OUTCOMES,"flush");
        } else if(hasStraight()) {
            return indexOf(OUTCOMES,"straight");
        } else if(hasThree()) {
            return indexOf(OUTCOMES,"three");
        } else if(hasTwo()) {
            return indexOf(OUTCOMES,"two");
        } else if(hasOne()) {
            return indexOf(OUTCOMES,"one");
        } else {

            finallyhand = this.doSafeCast(hand.clone(),Cards.class);
            return indexOf(OUTCOMES,"high");
        }
    }


    public int indexOf(char[] arr, char target) {
        for(int i=0; i<arr.length; i++) {
            if(arr[i]==target) {
                return i;
            }
        }
        return -1;
    }
    public int indexOf(String[] arr, String target) {
        for(int i=0; i<arr.length; i++) {
            if(arr[i].equals(target)) {
                return i;
            }
        }
        return -1;
    }

    public <T> LinkedList<T> doSafeCast(Object listObject, Class<T> type) {
        LinkedList<T> result = new LinkedList<T>();

        if (listObject instanceof List) {
            LinkedList<?> list = (LinkedList<?>)listObject;

            for (Object obj: list) {
                if (type.isInstance(obj)) {
                    result.add(type.cast(obj));
                }
            }
        }

        return result;
    }
}


class PokerCompare implements Comparator<PokerHand> {
    public PokerCompare() {}
    public int compare(PokerHand hand1, PokerHand hand2) {
        return hand1.compareTo(hand2);
    }
}
